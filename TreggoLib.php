<?php

class TreggoLib
{
    private $order;

    private function postToTreggo($endpoint, $curl_post_data)
    {
        $service_url = 'https://empresas.treggocity.com' . $endpoint;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curl_post_data));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        return $decoded;
    }

    private function getToTreggo($endpoint)
    {
        $service_url = 'https://empresas.treggocity.com' . $endpoint;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl, CURLOPT_POST, false);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        return $decoded;
    }

    public function __construct($api, $integracion)
    {
        $this->order = array(
            "client" => $api,
            "via" => $integracion,
            "type" => "ecommerce",
            "waypoints" => array(
                array(),
                array()
            )
        );
    }

    public function setPickUp($address, $door, $contact)
    {
        $this->order[waypoints][0] = array(
            "address" => $address
        );
    }

    public function setDelivery($address, $door, $contact)
    {
        $this->order[waypoints][1] = array(
            "address" => $address
        );
    }

    public function pricing()
    {
        return $this->postToTreggo("/1/order", $this->order);
    }

    public function dispatch($id)
    {
        return $this->getToTreggo("/1/order/".$id."/dispatch");
    }

    public function status($id)
    {
        return $this->getToTreggo("/1/order/".$id);
    }

    public function cancel($id)
    {
        return $this->getToTreggo("/1/order/".$id."/cancel");
    }
}