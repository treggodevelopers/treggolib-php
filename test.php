<?php
/**
 * Created by PhpStorm.
 * User: neko
 * Date: 23/1/18
 * Time: 04:29
 */
include_once 'TreggoLib.php';

$treggolib = new TreggoLib("0d0af120-54d1-42de-b6cb-8c031ae7f6f5","integracion");

$treggolib->setPickUp("Muñiz 1578, CABA, Argentina","","");
$treggolib->setDelivery("Doblas 618, CABA, Mendoza","","");

$order = $treggolib->pricing();
echo "Error al buscar da ".$order->errors[0]."\n";

$treggolib->setDelivery("Doblas 618, CABA, Argentina","","");
$order = $treggolib->pricing();
echo "Cotizacion correcta da ".$order->amount."\n";

$response = $treggolib->dispatch($order->id);
$order = $treggolib->status($order->id);
echo "Estado despues del alta ".$order->status->description."\n";

$response = $treggolib->cancel($order->id);
$order = $treggolib->status($order->id);
echo "Estado despues de la cancelacion ".$order->status->description."\n";