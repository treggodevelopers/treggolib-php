# README #

API Library for TREGGO

### Example usage ###

	include_once 'TreggoLib.php';

	// Set information
	$treggolib = new TreggoLib("0d0af120-54d1-42de-b6cb-8c031ae7f6f5","SamplePlugIn");
	$treggolib->setPickUp("Mu�iz 1578, CABA, Argentina","5to A","Nicolas");
	$treggolib->setDelivery("Doblas 618, CABA, Mendoza","PB 1","Andrea"); // Non existing adress

	// Get the pricing:
	$order = $treggolib->pricing();
	echo "Error since the address doesnt exists: ".$order->errors[0]."\n";

	$treggolib->setDelivery("Doblas 618, CABA, Argentina","PB 1","Andrea"); // Correct address
	$order = $treggolib->pricing();
	echo "Price of the shipping: ".$order->amount."\n";

	// Dispatch the order
	$response = $treggolib->dispatch($order->id);
	$order = $treggolib->status($order->id);
	echo "Status check: ".$order->status->description."\n";

### new TreggoLib ###

Parameters:

	new TreggoLib(API_KEY,INTEGRATION_NAME)

*API_KEY*: You can find your PAI_KEY on our plataform, at your profile

*INTEGRATION_NAME*: The identifyer of your implementation

### setPickUp / setDelivery ###

Parameters:

	$treggolib->setDelivery(ADDRESS,DOOR,CONTACT); // Correct address

*ADDRESS*: The adress of the pickup / delivery point

*DOOR*: The door number / floor / free text

*CONTACT*: Name of the person to contact, package to pick up / drop or information regarding the address


### pricing ###

	$order = $treggolib->pricing();

RESPONSE

*$order->id*: The id of the pricing / order

*$order->errors*: Array of errors found on the pricing

*$order->amount*: Price of the shipping

*$order->status->description*: The status of the order (changes throug the life cycle of the order)

### status ###

	$order = $treggolib->status($order->id);

RESPONSE

*$order->id*: The id of the pricing / order

*$order->errors*: Array of errors found on the pricing

*$order->amount*: Price of the shipping

*$order->status->description*: The status of the order (changes throug the life cycle of the order)

### dispatch ###

	$response = $treggolib->dispatch($order->id);
